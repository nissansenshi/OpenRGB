/*-----------------------------------------*\
|  RGBController_DrevoCaliburV2.h           |
|                                           |
|  Generic RGB Interface for Drevo RGB      |
|  keyboard devices                         |
|                                           |
|  Nissansenshi (N I S S A N) 15/4/2021     |
\*-----------------------------------------*/

#pragma once
#include "RGBController.h"
#include "DrevoCaliburv2TEController.h"

class RGBController_DrevoCaliburV2 : public RGBController
{
public:
    RGBController_DrevoCaliburV2(DrevoCaliburv2TEController * Drevo_ptr);
    ~RGBController_DrevoCaliburV2();

    void        SetupZones();

    void        ResizeZone(int zone, int new_size);
    
    void        DeviceUpdateLEDs();
    void        UpdateZoneLEDs(int zone);
    void        UpdateSingleLED(int led);

    void        SetCustomMode();
    void        DeviceUpdateMode();

private:
    DrevoCaliburv2TEController*    Drevo;
};