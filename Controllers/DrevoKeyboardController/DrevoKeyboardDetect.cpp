/*--------------------------------------------*\
|  DrevoKeyboardDetetct.cpp                    |
|                                              |
|  Detect for Drevo keyboards                  |
|                                              |
|  Nissansneshi (N I S S A N) 14/04/2021       |
\*--------------------------------------------*/
#include "Detector.h"
#include "DrevoCaliburv2TEController.h"
#include "RGBController.h"
#include "RGBController_DrevoCaliburV2.h"
#include <hidapi/hidapi.h>

/*--------------------------------------------*\
| Drevo Vendor ID                              |
\*--------------------------------------------*/
#define Drevo_VID                       0x05AC

/*--------------------------------------------*\
| Keyaboard Product ids                        |
\*--------------------------------------------*/
#define DrevoCaliburV2TEPID                0x024f
/******************************************************************************************\
*                                                                                          *
*   DetectDrevoKeyboardControllers                                                         *
*                                                                                          *
*       Tests the USB address to see if a Drevo RGB Keyboard controller exists there.      *
*                                                                                          *
\******************************************************************************************/

void DetectDrevoKeyboardControllers(hid_device_info* info, const std::string& name)
{
    hid_device* dev = hid_open_path(info->path);
    if( dev )
    {
        DrevoCaliburv2TEController* controller = new DrevoCaliburv2TEController(dev, info->path);
        RGBController_DrevoCaliburV2* rgb_controller = new RGBController_DrevoCaliburV2Keyboard(controller);
        rgb_controller->name = name;
        ResourceManager::get()->RegisterRGBController(rgb_controller);
    }
}   /* DetectDuckyKeyboardControllers() */

REGISTER_HID_DETECTOR_I("Drevo Calibur V2 TE", DetectDrevoKeyboardControllers, Drevo_VID, DrevoCaliburV2TEPID, 1);
