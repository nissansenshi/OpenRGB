/*-----------------------------------------*\
|  RGBController_DrevoCaliburV2.cpp         |
|                                           |
|  Generic RGB Interface for Drevo RGB      |
|  keyboard devices                         |
|                                           |
|  Nissansenshi (N I S S A N) 15/4/2021     |
\*-----------------------------------------*/

#include "RGBController_DrevoCaliburV2.h"

//0xffffffff indicated an unused entry in matrix
#define NA  0xffffffff

static unsigned int matrix[5][16] =
{ 
  {  00,  05, 10, 15, 19, 23, 27, 31, 35, 39, 43, 48, 53, 56, 61},
  {  01,  06, 11, 16, 20, 24, 28, 32, 36, 40, 44, 49, 54, 57, 62},
  {  02,  07, 12, 17, 21, 25, 29, 33, 37, 41, 45, 50, NA, 58, 63}, 
  {  03,  08, 13, 18, 22, 26, 30, 34, 38, 42, 46, 51, NA, 59, 64},
  {  04,  09. 14, NA, NA, NA, 19, NA, NA, NA, 47, 52, 55, 60, 65}
};

static const char* zone_names[] =
{
  "Keyboard"
};

static zone_type zone_types[] =
{
  ZONE_TYPE_MATRIX,
};

static const unsigned int zone_sizes[] =
{
  66
};

static const char *led_names[] =
{
    "Key: Escape",
    "Key: Tab",
    "Key: Caps Lock",
    "Key: Left Shift",
    "Key: Left Control",
    
    "Key: 1",
    "Key: Q",
    "Key: A",
    "Key: Left Windows",
    
    "Key: 2",
    "Key: W",
    "Key: S",
    "Key: Z",
    "Key: Left Alt",
    
    "Key: 3",
    "Key: E",
    "Key: D",
    "Key: X",
    

    "Key: 4",
    "Key: R",
    "Key: F",
    "Key: C",
    

    "Key: 5",
    "Key: T",
    "Key: G",
    "Key: V",
    
    
    "Key: 6",
    "Key: Y",
    "Key: H",
    "Key: B",
    "Key: Space",
   
    "Key: 7",
    "Key: U",
    "Key: J",
    "Key: N",
    
    
    "Key: 8",
    "Key: I",
    "Key: K",
    "Key: M",
    
  
    "Key: 9",
    "Key: O",
    "Key: L",
    "Key: ,",
    
    
    "Key: 0",
    "Key: P",
    "Key: ;",
    "Key: .",
    "Key: Right Alt",
    
    "Key: -",
    "Key: [",
    "Key: '",
    "Key: /",
    "Key: /",
    "Key: ]",
    "Key: Right Shift",
    "Key: Right Fn",
    "Key: Backspace",
    "Key: Enter",
     "Key: Right Control",
    "Key: Insert",
    "Key: Delete",
    "Key: Left Arrow",
     "Key: Up Arrow",
    "Key: Down Arrow",
    "Key: Page Up",
    "Key: Page Down",
    "Key: Right Arrow",
    "Key: Home"
    "Key: End",
    "Key: Pause/Break",
};

RGBController_DrevoCaliburV2Keyboard::RGBController_DrevoCaliburV2(DrevoCaliburv2TEController* Drevo_ptr)
{
  Drevo = Drevo_ptr

  name        = "Drevo Calibur V2";
  vendor      = "Drevo";
  type        = DEVICE_TYPE_KEYBOARD;
  description = "Drevo Calibur V2"
  location    = Drevo->GetDeviceLocation();
  serial      = Drevo->GetSerialString();

  mode Direct;
  Direct.name       = "Direct";
  Direct.value      = 0xFFFF;
  Direct.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
  Direct.color_mode = MODE_COLORS_PER_LED;

  SetupZones();
}

RGBController_DrevoCaliburV2Keyboard::RGBController_DrevoCaliburV2Keyboard(DrevoCaliburv2TEController* Drevo_ptr)
 /*---------------------------------------------------------*\
    | Delete the matrix map                                     |
    \*---------------------------------------------------------*/
    for(unsigned int zone_index = 0; zone_index < zones.size(); zone_index++)
    {
        if(zones[zone_index].matrix_map != NULL)
        {
            delete zones[zone_index].matrix_map;
        }
    }

    delete Drevo;
}

void RGBController_DrevoCaliburV2Keyboard::SetupZones()
{
    /*---------------------------------------------------------*\
    | Set up zones                                              |
    \*---------------------------------------------------------*/
    unsigned int total_led_count = 0;
    for(unsigned int zone_idx = 0; zone_idx < 1; zone_idx++)
    {
        zone new_zone;
        new_zone.name               = zone_names[zone_idx];
        new_zone.type               = zone_types[zone_idx];
        new_zone.leds_min           = zone_sizes[zone_idx];
        new_zone.leds_max           = zone_sizes[zone_idx];
        new_zone.leds_count         = zone_sizes[zone_idx];
        new_zone.matrix_map         = new matrix_map_type;
        new_zone.matrix_map->height = 6;
        new_zone.matrix_map->width  = 23;
        new_zone.matrix_map->map    = (unsigned int *)&matrix_map;
        zones.push_back(new_zone);

        total_led_count += zone_sizes[zone_idx];
    }

    for(unsigned int led_idx = 0; led_idx < total_led_count; led_idx++)
    {
        led new_led;
        new_led.name = led_names[led_idx];
        leds.push_back(new_led);
    }

    SetupColors();
}
void RGBController_DrevoCaliburV2Keyboard::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_DrevoCaliburV2Keyboard::DeviceUpdateLEDs()
{
    unsigned char colordata[155*3];
    
    for(std::size_t color_idx = 0; color_idx < colors.size(); color_idx++)
    {
        colordata[(color_idx*3)+0] = RGBGetRValue(colors[color_idx]);
        colordata[(color_idx*3)+1] = RGBGetGValue(colors[color_idx]);
        colordata[(color_idx*3)+2] = RGBGetBValue(colors[color_idx]);
    }

    ducky->SendColors(colordata, sizeof(colordata));
}

void RGBController_DrevoCaliburV2Keyboard::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_DrevoCaliburV2Keyboard::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateLEDs();
}

void RGBController_DrevoCaliburV2Keyboard::SetCustomMode()
{

}

void RGBController_DrevoCaliburV2Keyboard::DeviceUpdateMode()
{

}
